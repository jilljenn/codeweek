# JJ's Code Week

Code RuneStone de la JJ's Code Week 2014.

## Instructions

    virtualenv .
    pip install -r requirements.txt
    cd 2014
    paver build
    cd build
    python3 -m http.server
