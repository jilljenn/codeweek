
============================================
Jour 6 – Plate-forme de questions & réponses
============================================

Sur le site de questions & réponses `Quora <https://www.quora.com>`_, quelqu'un a posé la question suivante :

    What was the one line JavaScript that president Obama wrote as part of the Hour of Code 2014?

Et la réponse de Hadi Partovi (`@hadip <https://twitter.com/hadip>`_), fondateur de `code.org <http://code.org>`_, est la suivante :

    moveForward(100);

Vous pouvez lire `la réponse détaillée ici <https://www.quora.com/What-was-the-one-line-JavaScript-that-president-Obama-wrote-as-part-of-the-Hour-of-Code-2014>`_, où l'on apprend que cette ligne a permis à Barack Obama de faire avancer Anna de *La Reine des neiges* pour qu'elle finisse de tracer un carré dans la glace. Je n'ai jamais entendu autant de bonnes choses dans une même phrase.

Site de questions & réponses
::::::::::::::::::::::::::::

Je parle de manière plus détaillée des `sites questions & réponses Stack Exchange sur ce post de blog <http://vie.jill-jenn.net/2011/08/08/comment-poser-une-question/>`_.

Le principe : des gens posent des questions liées à un sujet (l'informatique, la science-fiction, la cuisine, la langue anglaise), d'autres gens répondent, les questionneurs peuvent récompenser les répondeurs par **des points de réputation**, en acceptant leurs réponses ou en attribuant des +1. Plus on a de réputation, plus on a de privilèges (pouvoir éditer un message, pouvoir attribuer -1 à une réponse, pouvoir bloquer un utilisateur).

Pourquoi il faut copier ce modèle
=================================

Je pense que ce système serait un meilleur outil qu'un forum pour une communauté d'entraide liée à un domaine (par ex. les mathématiques) pour une communauté (par ex. les élèves de prépa) :

- les gens ne font que poser des questions, ça limite les digressions (si fréquentes sur les forums)
- il y a une régulation automatique par la communauté, pondérée par la réputation
- il y a un excellent outil de recherche pour savoir si une question a déjà été posée.

Bref, à un hackathon `NightScience <http://nightscience.org>`_ (organisé par le centre de recherches interdisciplinaires), notre groupe (constitué d'enseignants et de geeks) a décidé de traduire l'équivalent *open source* de Stack Exchange en français. Et c'est ainsi que naquit `Pitika <http://q.pitika.fr>`_, actuellement testé par des étudiants en biologie.

Ceux qui l'ont copié
====================

Yahoo! Answers
--------------

Le premier à avoir proposé un système similaire, je pense que c'est `Yahoo! Answers <https://answers.yahoo.com>`_.

Stack Exchange
--------------

Les premiers à avoir réfléchi en profondeur à l'outil, c'est `Stack Exchange <http://stackexchange.com>`_. Leurs fondateurs, `Joel Spolsky <http://www.joelonsoftware.com>`_ et `Jeff Atwood <http://www.codinghorror.com>`_ ne sont pas des crétins, tous les programmeurs vous le diront. Surtout Joel Spolsky. (Bon sang, je viens de voir que c'est sa boîte qui a fait l'outil de productivité `Trello <https://trello.com>`_.) Ils ont été jusqu'à permettre à la communauté de pouvoir voter pour des nouveaux thèmes de sites de questions & réponses. `So meta <http://xkcd.com/917/>`_. Ça s'appelle `Area 51 <http://area51.stackexchange.com>`_, et on peut y voir qu'un Stack Overflow en arabe est en discussion ; un autre sur la mythologie ; un autre sur le café.

Quora
-----

Ceux dont j'entends de plus en plus parler en ce moment, c'est `Quora <https://www.quora.com>`_, que je mentionnais au début de ce billet. Je ne me suis jamais autant fait soutirer d'informations en m'inscrivant sur un site. Je pense que ce qui fait leur succès, c'est qu'il y a des célébrités dessus. Et rien de tel que de pouvoir permettre à des gens de poser leurs questions de manière organisée à des célébrités. De plus, avec les informations qu'ils soutirent, ils sont à même de recommander des questions déjà posées dont la réponse est susceptible d'intéresser l'utilisateur. Par exemple, j'ai eu droit à :

- `As we start planning the next edition of Introduction to Algorithms (CLRS), what should we add and what should we remove? <https://www.quora.com/As-we-start-planning-the-next-edition-of-Introduction-to-Algorithms-CLRS-what-should-we-add-and-what-should-we-remove>`_
- `How do you explain Machine Learning and Data Mining to non Computer Science people? <https://www.quora.com/How-do-you-explain-Machine-Learning-and-Data-Mining-to-non-Computer-Science-people>`_
- `What do top Kaggle competitors focus on? <https://www.quora.com/?digest_story=1360878>`_

Serveurs jetables
:::::::::::::::::

Je finis par un avis plus technique.

Le problème avec des sites codés en Python (tels que l'équivalent open source de Stack Exchange), c'est qu'il est un peu plus difficile de les déployer que des sites codés par exemple en PHP, on peut rarement se contenter d'un accès FTP. On a besoin de plus de contrôle sur la machine qui héberge.

Il y a quelque temps, on a vu apparaître un site Web avec un bouton : « Get a Server ». Le principe : pendant 30 minutes vous avez accès complet à un serveur et une adresse IP et vous pouvez installer ce que vous voulez dessus (par exemple, un site en Python). Au bout de 30 minutes, il faut payer pour faire tenir le serveur plus longtemps, sinon la session s'autodétruit. Je trouve ce principe génial : avoir une véritable machine façon bac à sable, pour tester une installation.

`The possibilities are endless. <https://www.youtube.com/watch?v=nkufZ6P350M>`_ On pourrait par exemple imaginer une plate-forme de questions & réponses jetable, par exemple pendant une conférence. On clique sur un bouton, ça installe un site, les gens peuvent poser leurs questions ou répondre à d'autres (de manière plus organisée qu'un *live tweet*, par exemple) et juste avant l'autodestruction, un backup complet est effectué et un rapport est généré automatiquement, reprenant les questions et réponses les plus populaires.

Le site était http://instantserver.io, mais le service n'est aujourd'hui plus proposé. D'après `Hacker News <https://news.ycombinator.com/item?id=5861749>`_, des gens ont sûrement dû faire des mauvaises choses avec. Mais `Luc Rocher <https://rocher.lc>`_ vient de me faire découvrir que `Heroku <http://heroku.com>`_ permet de `déployer une application en cliquant sur un simple bouton <https://devcenter.heroku.com/articles/heroku-button>`_ (c'est plutôt récent).

    (23 h 47)

    <Luc> Alors, ça avance ?  

    <Jill-Jênn> Mouais. Reste 13 minutes. J'espère que tu vas le lire.  

    <Luc> Ouais, je le lirai.  

    <Jill-Jênn> Non mais pour de vrai. Je pense qu'il y a des choses susceptibles de t'intéresser dedans.  

    <Luc> Ben ouais. Y a mon nom.

.. raw:: html
    
    <div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = 'jilljenn';
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
